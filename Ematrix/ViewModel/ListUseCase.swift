//
//  ListUseCase.swift
//  Ematrix
//
//  Created by komal kamble on 26/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation
import Alamofire
class ListUseCase{
    func loadData(with email : String,callback:@escaping (_ error : Error?,_ success:Bool)->Void) {
        let param : Parameters = ["emailId":email]
        
        NetworkManager.sharedClient.sendPostRequest(url: API.list, parameters: param, header: nil, encoding: JSONEncoding.default) { (respose) in
          
            if let error = respose.error{
               callback(error,false)
                
            }else{
                if let responseResult = respose.result.value
                {
                    
                    if let data = responseResult as? [String:Any]{
                        if let resData = data["items"] as? [[String:Any]]{
                            UserDefaults.standard.set(email, forKey: "EMAIL")
                            
                            let list = ListGenerator.shared.adapt(data: resData)
                            let coreData = CoreDataDAO()
                            coreData.deleteAll()
                            for item in list{
                                coreData.addList(list: item)
                                
                            }
                            
                            callback(nil,true)
                            
                        }else{
                            callback(nil,false)

                        }
                    }else{
                        callback(nil,false)

                    }
                    
                    
                    
                }
            }
        }
        
    }
    
}
