//
//  ListViewModel.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
class ListViewModel{
    
    var listUseCase : ListUseCase?
    
    init(listUseCase : ListUseCase) {
        self.listUseCase = listUseCase
    }

    
    var data = Variable<[ListData]>([])
    var emailText = Variable<String>("")
    var emailErrorMessage = PublishSubject<String>()
    
    
    var buttonPressed: AnyObserver<Void> {
        return AnyObserver { [weak self] event in
            switch event {
            case .next:
                guard let strongSelf = self else {
                    return
                }
                strongSelf.validate()
            default:
                break
            }
        }
    }
    private func validate() {
        if emailText.value.isValidAsEmail() {
            loadData(with: emailText.value)
        } else {
            processErrorsIfNeeded()
        }
    }
    
    private func processErrorsIfNeeded() {
        if emailText.value.isValidAsEmail() == false {
            let errorMessage = "Error.InvalidEmail".localizable
            emailErrorMessage.onNext(errorMessage)
        }
    }
    

    var submitButtonEnabled: Observable<Bool> {
        return emailText.asObservable().map { email in
            email.hasAtLeastOneSymbol()
        }
    }
    
    
    // Load Data from Server and Insert it to Local storage
    func loadData(with email : String) {
//        signIn(with: email)
        
        listUseCase?.loadData(with: email, callback: { (error, result) in
            self.populateData()
        })
        
    }
    

    
    
    // Get Data From Local
    func populateData(){
        let coreData = CoreDataDAO()
        coreData.getList(completion: { (list) in
            self.data.value = list
        })
    }

}


