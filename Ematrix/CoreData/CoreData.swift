//
//  CoreData.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation
import CoreStore
class CoreDataDAO{
    
    
    func addList(list: ListData)
    {
        CoreStore.perform(asynchronous: { transaction in
            let item = transaction.create(Into<List>())
            item.emailId = list.emailId
            item.firstName = list.firstName
            item.lastName = list.lastName
            item.imageUrl = list.imageUrl
            
            
        }, completion: { result in
            
        })
    }
    
    func deleteAll(){
        CoreStore.perform(asynchronous: { transaction in
            try transaction.deleteAll(From<List>())
        }, completion: { result in
            
        })
    }
    
    
    
    
    func getList(completion:@escaping (_ list : [ListData])->Void){
        var cartProducts: [ListData] = []
        
        CoreStore.perform(asynchronous: { transaction in
            let items = try transaction.fetchAll(From<List>())
            if let itemBe = items{
                for m in itemBe{
                    cartProducts.append(CoreDataListAdapter.adapt(item: m)!)
                }
            }
        }, completion: { result in
            completion(cartProducts)

        })
    }
    
}


struct CoreDataListAdapter {
   
    static func adapt(item: List?) -> ListData? {
        guard let item = item else {
            return nil
        }
        
        let list = ListData()
        list.emailId = item.emailId ?? ""
        list.firstName  = item.firstName ?? ""
        list.lastName  = item.lastName ?? ""
        list.imageUrl = item.imageUrl
        
       
        
        return list
    }
}
