//
//  EmailViewController.swift
//  Ematrix
//
//  Created by komal kamble on 24/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EmailViewController: UIViewController {
    @IBOutlet private weak var emailTextField: UITextField!
    
    @IBOutlet private weak var submitButton: UIButton!

    
    var viewModel : ListViewModel?

    var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupViewModel()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        emailTextField.placeholder = "Placeholder.Email".localizable
        submitButton.setTitle("Button.Submit".localizable, for: .normal)
        submitButton.backgroundColor = UIColor.black
        submitButton.setTitleColor(UIColor.white, for: .normal)
        
    }
    
   private func setupViewModel(){
    let listUse = ListUseCase()
    viewModel = ListViewModel(listUseCase: listUse)
    
        emailTextField.rx.value.map({ $0 ?? "" })
            .bind(to: viewModel!.emailText)
            .disposed(by: disposeBag)
    
        viewModel!.emailErrorMessage
            .subscribe(onNext: { [weak self] errorMessage in
            guard let strongSelf = self else {
                return
            }
                strongSelf.showAlert(with: "Alert.Email".localizable)
            })
            .disposed(by: disposeBag)
    
        submitButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            strongSelf.view.endEditing(true)
            })
            .disposed(by: disposeBag)
    
        submitButton.rx.tap
            .bind(to: viewModel!.buttonPressed)
            .disposed(by: disposeBag)
    
        viewModel!.submitButtonEnabled
            .subscribe(onNext: { [weak self] enabled in
            guard let strongSelf = self else {
                return
            }
            strongSelf.submitButton.isEnabled = enabled
                if(enabled){
                  strongSelf.submitButton.backgroundColor = UIColor.black.withAlphaComponent(1.0)
                }else{
                   strongSelf.submitButton.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                }
            })
            .disposed(by: disposeBag)
    
    
    viewModel!.data.asObservable()
        .subscribe(onNext: { [weak self] list in
            guard let strongSelf = self else {
                return
            }
            print("The Data is --->",list)
            if(list.count > 0){
                strongSelf.pushToList()

            }
            
        })
        .disposed(by: disposeBag)
    

    }
    
    func pushToList(){
        
         if let email = UserDefaults.standard.object(forKey: "EMAIL") as? String
         {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
            let nav = UINavigationController(rootViewController: vc)
            vc.email = email
            self.present(nav, animated: true, completion: nil)
        }
      
    }
    
    func showAlert(with message: String) {
        
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
