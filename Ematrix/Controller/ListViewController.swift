//
//  ListViewController.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import UIKit
import RxSwift

class ListViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!

//    var list = [ListData]()
    
    var email = String()
    var viewModel : ListViewModel? //= ListViewModel()
    var disposeBag = DisposeBag()


    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        viewModel!.populateData()
        viewModel!.loadData(with: email)

        // Do any additional setup after loading the view.
    }
    
    
    private func setupViewModel() {
        let listUseCase = ListUseCase()
        
        viewModel = ListViewModel(listUseCase: listUseCase)

        viewModel!.data.asObservable()
            .subscribe(onNext: { [weak self] cartProducts in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.data.value.count ?? 0
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! ListTableViewCell
        
        if let data = viewModel?.data.value{
            cell.setupWith(data: data[indexPath.row])

        }

        
        return cell

        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       return 60//UITableView.automaticDimension
        
    }
    

}

