//
//  NetworkManager.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation
import Alamofire
class NetworkManager {
   
    
    public static let sharedClient = NetworkManager()

    
    func sendPostRequest(url : String, parameters : Parameters?,header :HTTPHeaders?,encoding:ParameterEncoding,completion:@escaping (_ respose : DataResponse<Any>)->Void){
        Alamofire.request(url, method: .post, parameters: parameters, encoding: encoding, headers: header).responseJSON { (respose) in
            
            completion(respose)
            
            }.responseString { response in
               switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        print(data)
                    }
                    
                case .failure(_):
                    break
                }
        }
    }
}
