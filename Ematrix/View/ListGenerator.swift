//
//  ListGenerator.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation
class ListGenerator {
    
    public static let shared = ListGenerator()

    func adapt(data:[[String:Any]])->[ListData]{
        
        var list = [ListData]()
        for dataBe in data{
            var listBe = ListData()
            
            if let emailId = dataBe["emailId"] as? String{
               listBe.emailId = emailId
            }
            if let firstName = dataBe["firstName"] as? String{
                listBe.firstName = firstName
            }
            if let imageUrl = dataBe["imageUrl"] as? String{
                listBe.imageUrl = imageUrl
            }
            if let lastName = dataBe["lastName"] as? String{
                listBe.lastName = lastName
            }
            
            list.append(listBe)
            
        }
        
        return list
        
    }
}
