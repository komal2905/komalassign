//
//  ListTableViewCell.swift
//  Ematrix
//
//  Created by komal kamble on 25/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import UIKit
import Kingfisher

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWith(data : ListData){
        var firstName = ""
        var lastName = ""
        if let firstN = data.firstName{
          firstName = firstN
        }
        if let lastN = data.lastName{
            lastName = lastN
        }
        
        
        nameLabel.text = firstName+" "+lastName
        emailLabel.text = data.emailId
        
        if let image = data.imageUrl{
            let url  = image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let imageUrl = URL(string:url ?? "")
            let placeHolder =  #imageLiteral(resourceName: "eUser")
            
            profileImageView.kf.setImage(with: imageUrl, placeholder: placeHolder, options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
