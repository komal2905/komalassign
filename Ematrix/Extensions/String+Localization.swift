//
//  String+Localization.swift
//  Ematrix
//
//  Created by komal kamble on 24/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation

extension String {
    var localizable: String {
        return NSLocalizedString(self, comment: "")
    }
}
