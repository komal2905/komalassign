//
//  String+Validator.swift
//  Ematrix
//
//  Created by komal kamble on 24/11/19.
//  Copyright © 2019 Coense. All rights reserved.
//

import Foundation

extension String {
    func isValidAsEmail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    func hasAtLeastOneSymbol() -> Bool {
        return !isEmpty
    }
}
